<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:api', 'siswa'])->group(function () {
    // route untuk matpel
    Route::get('/semua-matpel', 'MatpelController@index');
    Route::get('/kelas/{kelas}/matpel', 'MatpelController@index_by_kelas');
    Route::get('/kelas/{kelas}/matpel/{matpel}', 'MatpelController@show');
    Route::middleware('admin')->group(function () {
        Route::post('/kelas/{kelas}/matpel', 'MatpelController@store');
        Route::put('/kelas/{kelas}/matpel/{matpel}', 'MatpelController@update');
        Route::delete('/kelas/{kelas}/matpel/{matpel}', 'MatpelController@destroy');
    });


    // route untuk materi
    Route::get('/kelas/{kelas}/matpel/{matpel}/materi', 'MateriController@index');
    Route::get('/kelas/{kelas}/matpel/{matpel}/materi/{id}', 'MateriController@show');
    Route::middleware('admin')->group(function () {
        Route::post('/kelas/{kelas}/matpel/{matpel}/materi', 'MateriController@store');
        Route::put('/kelas/{kelas}/matpel/{matpel}/materi/{id}', 'MateriController@update');
        Route::delete('/kelas/{kelas}/matpel/{matpel}/materi/{materi}', 'MateriController@destroy');
    });
});
