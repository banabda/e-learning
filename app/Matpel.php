<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matpel extends Model
{
    protected $table = 'matpels';
    protected $guarded = [];

    public function materi(){
        return $this->hasMany(Materi::class,'matpel_id','id');
    }
}
