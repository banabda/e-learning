<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'min:3', 'max:255'], 
            'email' => ['required', 'email', 'unique:users,email'], 
            'password'  => ['required', 'string', 'min:6'], 
            'phone' => ['required', 'string', 'min:8', 'max:15', 'unique:users,phone'], 
            'kelas' => ['required'],
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone' => $request->phone,
            'kelas' => $request->kelas,
        ]);

        return response('You\'re now registered!');
    }
}
