<?php

namespace App\Http\Controllers;

use App\Http\Resources\MateriResource;
use App\Materi;
use App\Matpel;
use Illuminate\Http\Request;

class MateriController extends Controller
{

    public function index($kelas,$matpel)
    {
        return response()->json([
            'status' => 'sukses',
            'data' => Materi::where(['matpel_id'=>$matpel,'kelas'=>$kelas])->get()
        ]);;
    }
    // public function store(Request $request, $kelas, $matpel, $materi){

    // }
    public function store(Request $request, $kelas, $matpel_id)
    {
        $data = new Request([
            'matpel_id' => $matpel_id,
            'materi' => $request->materi
        ]);

        $data->validate([
            'materi' => ['required', 'min:3'],
            'matpel_id' => ['required', 'exists:App\Matpel,id'],
        ]);

        $matpel = Matpel::find($matpel_id);

        $materi = new Materi;
        $materi->kelas = $matpel->kelas;
        $materi->matpel_id = $data->matpel_id;
        $materi->materi = $data->materi;
        $materi->save();
        return response()->json([
            'status' => 'sukses',
            'message' => 'Penambahan Data Materi Berhasil',
            'data' => [
                'materi' => $materi,
            ]
        ]);
    }

    public function show($kelas, $matpel, $id)
    {
        if (Materi::find($id) != null)
            return response()->json([
                'status' => 'sukses',
                'message' => "Berhasil melihat Data Materi dengan ID : $id",
                'data' => Materi::find($id)
            ]);
        return response()->json([
            'status' => 'sukses',
            'message' => 'Data tidak di temukan'
        ]);
    }

    public function update(Request $request, $kelas, $matpel, $materi)
    {
        if (Materi::find($materi)) {

            $data = new Request([
                'matpel_id' => $matpel,
                'materi' => $request->materi
            ]);
    
            $data->validate([
                'materi' => ['required', 'min:3'],
                'matpel_id' => ['required', 'exists:App\Matpel,id'],
            ]);
    
            $matpel = Matpel::find($matpel);
                // dd($materi);
            $materi = Materi::find($materi);
            $materi->kelas = $matpel->kelas;
            $materi->matpel_id = $data->matpel_id;
            $materi->materi = $data->materi;
            $materi->save();
            return response()->json([
                'status' => 'sukses',
                'message' => 'Penambahan Data Materi Berhasil',
                'data' => [
                    'materi' => $materi,
                ]
            ]);
        }
        return response()->json([
            'status' => 'sukses',
            'message' => 'Data tidak di temukan'
        ]);
    }

    public function destroy($kelas, $matple, Materi $materi)
    {
        $materi->delete();
        return response()->json([
            'status' => 'sukses',
            'message' => 'Data Materi Berhasil di hapus',
        ]);
    }
}
