<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Matpel;
use App\Http\Resources\MatpelResource;

class MatpelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $matpels = Matpel::orderBy('kelas','asc')->get();

        return MatpelResource::collection($matpels);
    }

    public function index_by_kelas($kelas)
    {
        $matpels = Matpel::where('kelas',$kelas)->get();

        return MatpelResource::collection($matpels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$kelas)
    {
        $request->validate([
            'nama'=>['required','min:3'],
        ]);

        $matpel = Matpel::create([
            'nama'=> request('nama'),
            'kelas'=> $kelas
        ]);

        return new MatpelResource($matpel);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kelas,$matpel_id)
    {
        $matpel = Matpel::where(['id'=>$matpel_id, 'kelas'=>$kelas])->first();

        return new MatpelResource($matpel);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kelas, $matpel_id)
    {
        $request->validate([
            'nama'=>['required']
        ]);

        Matpel::find($matpel_id)->update([
            'nama' => request('nama')
        ]);

        $matpel = Matpel::find($matpel_id);

        return new MatpelResource($matpel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kelas, $matpel_id)
    {
        $matpel = Matpel::find($matpel_id);

        if($matpel){
            $matpel->delete();
            return 'Matpel berhasil dihapus';
        }
        else{
            return 'Matpel tidak ditemukan';
        }
    }
}
