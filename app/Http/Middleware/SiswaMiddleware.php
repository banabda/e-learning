<?php

namespace App\Http\Middleware;

use Closure;

class SiswaMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = auth()->user()->role;
        $kelas_route = $request->route('kelas');
        $kelas_user = auth()->user()->kelas;
        
        if($role=='admin'){
            return $next($request);
        }
        elseif($kelas_route == $kelas_user){
            return $next($request);
        }
        elseif($request->server->all()['REQUEST_URI'] == "/api/semua-matpel"){
            return $next($request);
        }

        abort(403);
    }
}
