<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    protected $table = 'materis';
    protected $guarded = [];
    public function matpel()
    {
        return $this->belongsTo(Matpel::class);
    }
}
